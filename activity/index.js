let number = Number(prompt("Enter a number: "));
console.log("The number you provided is: " + number);
for (let loopNumber = number; loopNumber > 0; loopNumber --){
	
	if (loopNumber % 10 === 0 && loopNumber > 50){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	
	if (loopNumber % 5 === 0 && !(loopNumber % 10 === 0) ){
		console.log(loopNumber)
		continue;
	}	
	

	if (loopNumber <= 50){
		console.log("The current value is at 50. Terminating the loop");
		break;
	}

}

let variableName = "supercalifragilisticexpialidocious";
let vowel = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
let newVariable = "";

for (let i = 0; i < variableName.length; i++){

	if (!vowel.includes(variableName[i])){
		newVariable += variableName[i];

	}

}
console.log(variableName);
console.log(newVariable);
